#!/bin/bash
#
#  submit by  sbatch example_ExtractBarcodes.sh
#SBATCH --job-name=example  #specify the job name
#SBATCH --ntasks=4          #how many cpus are requested
#SBATCH --nodes=1           #run on one node, importand if you have more than 1 ntasks
#SBATCH --time=00:20:00     #maximum walltime, here 20min
#SBATCH --mem=250G          #maximum requested memory, here 250G
#SBATCH --error=insert.%J.err     #  write std out and std error to these files
#SBATCH --output=insert.%J.out

#SBATCH --partition=standard  #  which partition? there are global,testing,highmem,standard,fast


#  add your code here:
###############################################################################################################################################################################################
## Developed by Loukas Theodosiou: last update 10.09.2020 Credits to Demetris Taliadoros for his unprecented help                                                                                                                                    ##
## USE THIS PIPELINE TO: 1) TRIM SEQUENCING ADDAPTERS, TRIM ON LENGHT AND QUALITY 2) ALIGNE READS AT REFERENCE GENOME WITH breseq
###############################################################################################################################################################################################

#path from working directory to directory that you wish the job to be done
path=/path/to/data/     

###############################################
#   _______   ________   ______   _______  
#  /       \ /        | /      \ /       \ 
# $$$$$$$  |$$$$$$$$/ /$$$$$$  |$$$$$$$  |
# $$ |__$$ |$$ |__    $$ |__$$ |$$ |__$$ |
# $$    $$/ $$    |   $$    $$ |$$    $$< 
# $$$$$$$/  $$$$$/    $$$$$$$$ |$$$$$$$  |
# $$ |      $$ |_____ $$ |  $$ |$$ |  $$ |
# $$ |      $$       |$$ |  $$ |$$ |  $$ |
# $$/       $$$$$$$$/ $$/   $$/ $$/   $$/ 
# 
##############################################                                      

#software
pear=/path/to/PEAR/

#--------------------------
# ┌┬┐┌─┐┬─┐┌─┐┌─┐   ┌─┐┌─┐
# │││├┤ ├┬┘│ ┬├┤    │ ┬┌─┘
# ┴ ┴└─┘┴└─└─┘└─┘  o└─┘└─┘
#--------------------------

# Create necessary directories
mkdir ${path}merged
merged_data=${path}merged/

## Merge fastq.gz files

cd ${path}
for each in *R1_001.fastq.gz
do
${pear}pear -f ${each} -r ${each%_R1.fastq}_R2.fastq.gz -o ${merged_data}${each%_R1.fastq}
done

#-----------------------------------------------
# ┌─┐┬  ┬  ┌─┐┌─┐┌─┐┌┬┐┌─┐  ┌─┐┬ ┬┌┬┐┌─┐┬ ┬┌┬┐
# ├─┤│  │  │ ││  ├─┤ │ ├┤   │ ││ │ │ ├─┘│ │ │ 
# ┴ ┴┴─┘┴─┘└─┘└─┘┴ ┴ ┴ └─┘  └─┘└─┘ ┴ ┴  └─┘ ┴ 
#----------------------------------------------

### make dirs for the pear outputs
mkdir ${merged_data}assembled_reads/
assembled_dir=${merged_data}assembled_reads/
cd ${merged_data}
mv *.assembled.fastq ${assembled_dir}

mkdir ${merged_data}discarded_reads/
discarded_dir=${merged_data}discarded_reads/
cd ${merged_data}
mv *.discarded.fastq ${discarded_dir}

mkdir ${merged_data}unassembled_reads/
unassembled_dir=${merged_data}unassembled_reads/
cd ${merged_data}
mv *.unassembled.fastq ${unassembled_dir}

#----------------------------------------------------------------
#     ______                        __                         
#    /      \                      |  \                        
#   |  $$$$$$\ ______    _______  _| $$_     ______    _______ 
#   | $$_  \$$|      \  /       \|   $$ \   /      \  /       \
#   | $$ \     \$$$$$$\|  $$$$$$$ \$$$$$$  |  $$$$$$\|  $$$$$$$
#   | $$$$    /      $$ \$$    \   | $$ __ | $$  | $$| $$      
#   | $$     |  $$$$$$$ _\$$$$$$\  | $$|  \| $$__| $$| $$_____ 
#   | $$      \$$    $$|       $$   \$$  $$ \$$    $$ \$$     \
#    \$$       \$$$$$$$ \$$$$$$$     \$$$$   \$$$$$$$  \$$$$$$$
#                                                | $$          
#                                                | $$          
#                                                 \$$          
#----------------------------------------------------------------

## check the quality of the assembled merged files 
fastqc=/path/fastQC/

#-----------------------
# ┌─┐┌─┐┌─┐┌┬┐┌─┐ ┌─┐
# ├┤ ├─┤└─┐ │ │─┼┐│  
# └  ┴ ┴└─┘ ┴ └─┘└└─┘
#-----------------------

# Create necessary directories
mkdir ${assembled_dir}fastQC # here I ll put all fastQC data
fastQC_dir=${assembled_dir}fastQC/

cd ${assembled_dir}
for each in *.fastq
do
${fastqc}fastqc ${each} -o ${fastQC_dir}
done

#-----------------------
# ┌┬┐┬ ┬┬ ┌┬┐┬┌─┐ ┌─┐
# ││││ ││  │ ││─┼┐│  
# ┴ ┴└─┘┴─┘┴ ┴└─┘└└─┘
#-----------------------

mkdir ${assembled_dir}fastQC/mutliqc_reports  # here I ll move all the html and zip files


#---------------------------------------------------------------------------------
#              __            __                                      ______  
#             |  \          |  \                                    /      \ 
# ______ ____   \$$ _______   \$$ ______ ____    ______    ______  |  $$$$$$\
# |      \    \ |  \|       \ |  \|      \    \  |      \  /      \  \$$__| $$
# | $$$$$$\$$$$\| $$| $$$$$$$\| $$| $$$$$$\$$$$\  \$$$$$$\|  $$$$$$\ /      $$
# | $$ | $$ | $$| $$| $$  | $$| $$| $$ | $$ | $$ /      $$| $$  | $$|  $$$$$$ 
# | $$ | $$ | $$| $$| $$  | $$| $$| $$ | $$ | $$|  $$$$$$$| $$__/ $$| $$_____ 
# | $$ | $$ | $$| $$| $$  | $$| $$| $$ | $$ | $$ \$$    $$| $$    $$| $$     \
#  \$$  \$$  \$$ \$$ \$$   \$$ \$$ \$$  \$$  \$$  \$$$$$$$| $$$$$$$  \$$$$$$$$
#                                                         | $$                
#                                                         | $$                
#                                                          \$$
#---------------------------------------------------------------------------------

reference_file=./Reference_barcodes.fasta

#take the assembled reads 
cd ${assembled_dir}

# Create necessary directories
mkdir ${assembled_dir}minimap2_output

out_dir=${assembled_dir}minimap2_output/

cd ${assembled_dir}
for each in *.fastq
do
   minimap2 -a ${reference_file} ${each} > ${out_dir}${each}.sam 
   samtools view -S -b ${out_dir}${each}.sam > ${out_dir}${each}.bam 
   samtools sort ${out_dir}${each}.bam  -o ${out_dir}${each}_sorted.bam  
   samtools index ${out_dir}${each}_sorted.bam 

  done

cd ${out_dir}
for each in *_sorted.bam; do
    printf '%s\n' "${each%.*}"
    samtools view -c -F 4 "$each"  # get the unmapped reads
    samtools view -c -f 4 "$each"  # get the mapped reads
done |
awk -v OFS='\t' '{a[NR%3]=$0} NR%3==0{print a[1], a[2], a[0]}' > "sum_bams_Stat_rep2.txt"

#---------------
#  _______  
# |       \ 
# | $$$$$$$\
# | $$__| $$
# | $$    $$
# | $$$$$$$\
# | $$  | $$
# | $$  | $$
#  \$$   \$$
#---------------

module load R/4.1.2

cd ./script/location/

Rscript ${your_name}.R 
echo "finish"
