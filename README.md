# Barcoding _Pseudomonoas fluorescens SBW25_

<img src="./Figures/logo.png" alt="J" width="600"/>


## Repository Structure

```
┬
└─▢ $SBW25-barcoding   ▢ home directory
  ├─▢ $data            ▢ data folder
  | |─▢ raw_data       ▢ fastq files
  | |─▢ merged         ▢ merged fastq  
  │ ├─▢ bam            ▢ bam files 
  │ ├─▢ reference      ▢ contains the reference barcode
  │ ├─▢ plasmids       ▢ plasmid sequences
  ├─▢ $code            ▢ code folder
  ├─▢ $Figures         ▢ Illustrations and code ouput
  └─▢ README.md        ▢ most recent standard output file
```

- `merged` : This is were the merged files with PEAR are stored.
- `bam` : This is where the sorted bam files and their index is stored.
- `code` : Here are stored all codes for extractign barcodes and creating figures.

## Extract raw barcode sequences step by step 

### 1.Check quality of the raw data with [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)

```
cd data                        #go to your data folder
mkdir fastQC                   #create a fastQC folder

for each in *fastq             #run a for loop 
do 
fastQC ${each} -o ./fastQC/    #send the output of the fastQC software to the fastQC folder 
done

```

You can sumarise the output of the [fastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) [1] using the [multiQC](https://multiqc.info/) [2]software. Optionally you can 
[Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic) [3] to trim low quality data. However many mappers such a [minimap](https://github.com/lh3/minimap2) [4] are really good masking low quality data.

```
cd data/fastQC/

mutliqc . 

```

### 2. Merge files with [PEAR](https://cme.h-its.org/exelixis/web/software/pear/doc.html)

To merge the paired end reads and keep only the one with the highest quality we used [PEAR](https://github.com/tseemann/PEAR) [5].

```
cd data                                             #the path to your data 

for each in *_R1.fastq                              #for loop to merge all pair-end files
do 
pear -f ${each} -r ${each%_R1.fastq}_R2.fastq - o ./merged/${each%_R1.fastq}_ANC
done 
```

### 3. Map merged data to the referece barcode with [minimap](https://github.com/lh3/minimap2)

```
cd merged                                           # go in the folder where merged files are and use the assembled files 
mkdir bam                                           # make a bam folder
out_dir=/data/merged/bam/                           # specify where you want the output of minimap to go 
reference_file=`path to reference file` eg., /home/reference/reference_barcodes fasta 

for each in * assembled.fastq
do 
  minimap2 -a ${reference_file} ${each} > ${out_dir}${each}.sam              # map data to reference barocde 
   samtools view -S -b ${out_dir}${each}.sam > ${out_dir}${each}.bam         # covert sam to bam
   samtools sort ${out_dir}${each}.bam  -o ${out_dir}${each}_sorted.bam      # sort bam 
   samtools index ${out_dir}${each}_sorted.bam                               # index sorted bam files

  done

```
It is always recommended to do statistics in your bam files. Here we check how many sequences have mapped to barcode and how many not

```
cd ${out_dir}
for each in *_sorted.bam; do
    printf '%s\n' "${each%.*}"
    samtools view -c -F 4 "$each"   # get the unmapped reads
    samtools view -c -f 4 "$each"   # get the mapped reads
done |
awk -v OFS='\t' '{a[NR%3]=$0} NR%3==0{print a[1], a[2], a[0]}' > "sum_bams_Stat_rep2.txt"

```
### 4. Extract barcodes from the bam files using [R](https://www.r-project.org/)

We used [R](https://www.r-project.org/) [6] version 4.2.1 and the bioconductor package [GenomicAlignments](https://bioconductor.org/packages/release/bioc/html/GenomicAlignments.html) [7] to extract barcode reads from the bam files. 

```
setwd("/path/to/bam/files")
library(GenomicAlignments)

list.files()
files=list.files(pattern = "*fastq_sorted.bam$") 
files

for(i in 1:length(files)) {
  
  stack <- stackStringsFromBam(files[i],
                               param=GRanges("Reference_barcodes:54-78"),
                               use.names = TRUE,
                               what = "seq")
  data <- stack %>%
    as.data.frame() %>%
    dplyr::rename(barcode=x) %>%
    group_by(barcode) %>%
    dplyr::count() %>%
    arrange(desc(n)) %>%
    dplyr::rename(counts=n) %>%
    mutate(length_barcode=str_length(barcode))
  
  write.table(data, quote=FALSE, sep=",", sub(".assembled.fastq_sorted.bam","_barcodes.txt", files[i])) # it writes in your working directory txt files with the baroceds 
  
}
```
### 5. Exploratory Data Analysis (EDA) of the extracted barcode reads using [R](https://www.r-project.org/)
You can find an example script for submitting the code above in a SLURM cluster in `code/example_ExtractBarcodes.sh`.

**Disclaimer:** Fully processing the extracted raw barcode reads requires more extensive descriptions and coding, which is part of a forthcoming study. Here 
we provide an exploratory analysis pipeline, `/code/EDA_SBW25barcodes_final_version.R`, of the raw barcode reads we have extracted from the bam files.

All the details about the R version and R packages we have used can be found in here 
```
> sessionInfo()
R version 4.2.1 (2022-06-23)
Platform: x86_64-apple-darwin17.0 (64-bit)
Running under: macOS Monterey 12.6

Matrix products: default
LAPACK: /Library/Frameworks/R.framework/Versions/4.2/Resources/lib/libRlapack.dylib

locale:
[1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8

attached base packages:
[1] grid      stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] ltc_0.0.0.9000    devtools_2.4.4    usethis_2.1.6     here_1.0.1        scales_1.2.1      ggpubr_0.4.0     
 [7] ggpmisc_0.5.0     ggpp_0.4.4        UpSetR_1.4.0      collapse_1.8.8    tidytable_0.8.1   patchwork_1.1.2  
[13] forcats_0.5.2     stringr_1.4.1     dplyr_1.0.10      purrr_0.3.4       readr_2.1.2       tidyr_1.2.0      
[19] tibble_3.1.8      ggplot2_3.3.6     tidyverse_1.3.2   data.table_1.14.2 systemfonts_1.0.4

loaded via a namespace (and not attached):
 [1] fs_1.5.2            lubridate_1.8.0     httr_1.4.4          rprojroot_2.0.3     profvis_0.3.7      
 [6] tools_4.2.1         backports_1.4.1     utf8_1.2.2          R6_2.5.1            DBI_1.1.3          
[11] colorspace_2.0-3    urlchecker_1.0.1    withr_2.5.0         prettyunits_1.1.1   processx_3.7.0     
[16] tidyselect_1.1.2    gridExtra_2.3       curl_4.3.2          compiler_4.2.1      cli_3.4.0          
[21] rvest_1.0.3         quantreg_5.94       gt_0.7.0            SparseM_1.81        xml2_1.3.3         
[26] labeling_0.4.2      callr_3.7.2         digest_0.6.29       pkgconfig_2.0.3     htmltools_0.5.3    
[31] sessioninfo_1.2.2   dbplyr_2.2.1        fastmap_1.1.0       htmlwidgets_1.5.4   rlang_1.0.5        
[36] readxl_1.4.1        rstudioapi_0.13     shiny_1.7.2         farver_2.1.1        generics_0.1.3     
[41] jsonlite_1.8.0      car_3.1-0           googlesheets4_1.0.1 magrittr_2.0.3      Matrix_1.4-1       
[46] Rcpp_1.0.9          munsell_0.5.0       fansi_1.0.3         abind_1.4-5         lifecycle_1.0.2    
[51] stringi_1.7.8       carData_3.0-5       MASS_7.3-58.1       pkgbuild_1.3.1      plyr_1.8.7         
[56] parallel_4.2.1      promises_1.2.0.1    crayon_1.5.1        miniUI_0.1.1.1      lattice_0.20-45    
[61] haven_2.5.1         splines_4.2.1       hms_1.1.2           ps_1.7.1            pillar_1.8.1       
[66] ggsignif_0.6.3      pkgload_1.3.0       reprex_2.0.2        glue_1.6.2          remotes_2.4.2      
[71] modelr_0.1.9        tweenr_2.0.2        vctrs_0.4.1         tzdb_0.3.0          httpuv_1.6.5       
[76] MatrixModels_0.5-0  cellranger_1.1.0    polyclip_1.10-0     gtable_0.3.1        assertthat_0.2.1   
[81] cachem_1.0.6        ggforce_0.3.4       mime_0.12           xtable_1.8-4        broom_1.0.0        
[86] rstatix_0.7.0       later_1.3.0         survival_3.4-0      googledrive_2.0.0   gargle_1.2.0       
[91] memoise_2.0.1       ellipsis_0.3.2 
```



### References 

1.  Andrews S. (2010). FastQC: a quality control tool for high throughput sequence data. Available online at: http://www.bioinformatics.babraham.ac.uk/projects/fastqc <br>
2. P. Ewels, M. Magnusson, S. Lundin, and M. Käller, “MultiQC: summarize analysis results for multiple tools and samples in a single report,” Bioinformatics, vol. 32, no. 19, pp. 3047–3048, Jun. 2016, doi: 10.1093/bioinformatics/btw354.  <br>
3. A. M. Bolger, M. Lohse, and B. Usadel, “Trimmomatic: a flexible trimmer for Illumina sequence data,” Bioinformatics, vol. 30, no. 15, pp. 2114–2120, Apr. 2014, doi: 10.1093/bioinformatics/btu170. <br>
4. H. Li, “New strategies to improve minimap2 alignment accuracy,” Bioinformatics, vol. 37, no. 23, pp. 4572–4574, Oct. 2021, doi: 10.1093/bioinformatics/btab705.  <br>
5. J. Zhang, K. Kobert, T. Flouri, and A. Stamatakis, “PEAR: a fast and accurate Illumina Paired-End reAd mergeR,” Bioinformatics, vol. 30, no. 5, pp. 614–620, Oct. 2013, doi: 10.1093/bioinformatics/btt593.  <br>
6. R Core Team (2020). R: A language and environment for statistical computing. R Foundation for Statistical Computing, Vienna, Austria. URL https://www.R-project.org/  <br>
7. M. Lawrence et al., “Software for Computing and Annotating Genomic Ranges,” PLoS Computational Biology, vol. 9, no. 8, p. e1003118, Aug. 2013, doi: 10.1371/journal.pcbi.1003118.





